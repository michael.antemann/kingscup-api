import { Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { ObjectId } from 'mongodb';
import { User } from 'src/users/user.entity';
import { UsersService } from 'src/users/users.service';
import { Repository } from 'typeorm';
import { RulesetDto } from './ruleset.dto';
import { Ruleset } from './ruleset.entity';

@Injectable()
export class RulesetsService {
  constructor(
    @InjectRepository(Ruleset) private repo: Repository<Ruleset>,
    private usersService: UsersService,
  ) {}

  async create({ name, rules }: RulesetDto, user: User): Promise<Ruleset> {
    const ruleset = await this.repo.save({ name, rules });
    const newIds = [...user.rulesetIds, ruleset.id.toString()];
    await this.usersService.patchRulesetIds(user.id, newIds);
    return ruleset;
  }

  async findByIds(ids: string[]): Promise<Ruleset[]> {
    return await this.repo.findByIds(ids.map(id => new ObjectId(id)));
  }
}
