import { Rule } from './rule';

export class RulesetDto {
  name: string;
  rules: Rule[];
}
