import { ApiProperty } from '@nestjs/swagger';

export class Rule {
  @ApiProperty()
  cardNo: number;
  @ApiProperty()
  title: string;
  @ApiProperty()
  text: string;
}
