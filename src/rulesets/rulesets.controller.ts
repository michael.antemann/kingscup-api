import { Body, Controller, Get, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { User } from 'src/users/user.entity';
import { UsersService } from 'src/users/users.service';
import { RulesetDto } from './ruleset.dto';
import { Ruleset } from './ruleset.entity';
import { RulesetsService } from './rulesets.service';

@ApiTags('rulesets')
@Controller('rulesets')
@UseGuards(AuthGuard('jwt'))
export class RulesetsController {
  constructor(public rulesetsService: RulesetsService, public usersService: UsersService) {}

  @Get('me')
  async getUserRulesets(@Request() req: { user: User }): Promise<Ruleset[]> {
    const { id } = req.user;
    const rulesetIds = await this.usersService.findRulesetIds(id);
    return await this.rulesetsService.findByIds(rulesetIds.map(oId => oId.toString()));
  }

  @Post()
  async createRuleset(@Body() body: RulesetDto, @Request() req): Promise<Ruleset> {
    const { name, rules } = body;
    return await this.rulesetsService.create({ name, rules }, req.user);
  }
}
