import { Column, Entity, ObjectIdColumn } from 'typeorm';
import { Rule } from './rule';

@Entity()
export class Ruleset {
  @ObjectIdColumn()
  id: string;

  @Column()
  name: string;

  @Column()
  rules: Rule[];
}
