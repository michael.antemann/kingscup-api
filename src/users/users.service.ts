import { ConflictException, Injectable } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import * as bcrypt from 'bcrypt';
import { Repository } from 'typeorm';
import { UserRole } from './user-role.enum';
import { User } from './user.entity';

@Injectable()
export class UsersService {
  constructor(
    @InjectRepository(User)
    private readonly repo: Repository<User>,
  ) {}

  async create(username: string, password: string): Promise<User> {
    const hash = bcrypt.hashSync(password, 10);
    const usernameTaken = await this.findByUsername(username);
    const roles = [UserRole.User];
    const rulesetIds = [];
    if (usernameTaken) {
      throw new ConflictException('username already taken');
    }
    return await this.repo.save({ username, hash, roles, rulesetIds });
  }

  async findByUsername(username: string): Promise<User> {
    return await this.repo.findOne({ username });
  }

  async findRulesetIds(userId: string): Promise<string[]> {
    const user = await this.repo.findOne(userId);
    return user.rulesetIds;
  }

  async getAll(): Promise<User[]> {
    return await this.repo.find();
  }

  async patchRulesetIds(userId: string, rulesetIds: string[]): Promise<User> {
    console.log(rulesetIds);
    const user = await this.repo.findOne(userId);
    console.log(user);
    user.rulesetIds = [...user.rulesetIds, ...rulesetIds];
    return await this.repo.save(user);
  }

  async deleteById(id: string): Promise<any> {
    const user = await this.repo.findOne(id);
    return await this.repo.delete(user);
  }
}
