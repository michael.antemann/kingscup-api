import { Body, Controller, Delete, Get, Param, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiTags } from '@nestjs/swagger';
import { AdminGuard } from './admin.guard';
import { RegisterDto } from './dto/register.dto';
import { User } from './user.entity';
import { UsersService } from './users.service';

@ApiTags('users')
@Controller('users')
export class UsersController {
  constructor(private usersService: UsersService) {}

  @Get('profile')
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  getProfile(@Request() req) {
    return req.user;
  }
  @Post()
  async create(@Body() body: RegisterDto): Promise<User> {
    const { username, password } = body;
    return await this.usersService.create(username, password);
  }

  @Get()
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async getAll() {
    return await this.usersService.getAll();
  }

  @Delete(':id')
  @UseGuards(AuthGuard('jwt'), AdminGuard)
  async delete(@Param('id') id: string) {
    await this.usersService.deleteById(id);
  }
}
