import { Column, Entity, ObjectIdColumn } from 'typeorm';

@Entity()
export class User {
  @ObjectIdColumn()
  id: string;

  @Column()
  username: string;

  @Column()
  roles: string[];

  @Column()
  rulesetIds: string[];

  @Column()
  hash: string;
}
