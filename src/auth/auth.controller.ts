import { Controller, Post, Request, UseGuards } from '@nestjs/common';
import { AuthGuard } from '@nestjs/passport';
import { ApiBody, ApiTags } from '@nestjs/swagger';
import { AuthService } from './auth.service';
import { LoginDto } from './dto/login.dto';

@ApiTags('auth')
@Controller('auth')
export class AuthController {
  constructor(private readonly authService: AuthService) {}
  @ApiBody({ type: LoginDto })
  @UseGuards(AuthGuard('local')) // local.strategy username/password
  @Post('login')
  async login(@Request() req) {
    return this.authService.login(req.user); // return jwt
  }
}
