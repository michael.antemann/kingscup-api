import { Injectable } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import * as bcrypt from 'bcrypt';
import { User } from 'src/users/user.entity';
import { UsersService } from '../users/users.service';

@Injectable()
export class AuthService {
  constructor(
    private readonly usersService: UsersService,
    private readonly jwtService: JwtService,
  ) {}

  async validateUser(username: string, password: string): Promise<any> {
    const user = await this.usersService.findByUsername(username);
    if (!user) {
      return null;
    }
    const correctPassword = bcrypt.compareSync(password, user.hash);
    if (correctPassword) {
      return user;
    }
    return null;
  }

  async login(user: User) {
    delete user.hash;
    const payload = { user };
    const token = this.jwtService.sign(payload);
    return { token };
  }
}
