import { Module } from '@nestjs/common';
import { TypeOrmModule } from '@nestjs/typeorm';
import { AppController } from './app.controller';
import { AppService } from './app.service';
import { AuthModule } from './auth/auth.module';
import { Ruleset } from './rulesets/ruleset.entity';
import { RulesetsModule } from './rulesets/rulesets.module';
import { User } from './users/user.entity';
import { UsersModule } from './users/users.module';

@Module({
  imports: [
    TypeOrmModule.forRoot({
      type: 'mongodb',
      host: 'localhost',
      port: 27017,
      database: 'kingscup',
      entities: [User, Ruleset],
      useUnifiedTopology: true,
      synchronize: true,
    }),
    AuthModule,
    UsersModule,
    RulesetsModule,
  ],
  controllers: [AppController],
  providers: [AppService],
})
export class AppModule {}
