FROM node:13-alpine
COPY ./package.json /
CMD ["npm", "run", "start:prod"]
